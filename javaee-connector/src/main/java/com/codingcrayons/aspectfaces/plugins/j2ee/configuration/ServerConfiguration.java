/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.plugins.j2ee.configuration;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletContext;

import com.codingcrayons.aspectfaces.cache.ResourceCache;
import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.exceptions.AFFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileAccessException;
import com.codingcrayons.aspectfaces.exceptions.TemplateFileNotFoundException;
import com.codingcrayons.aspectfaces.util.Files;
import com.codingcrayons.aspectfaces.util.Strings;

public class ServerConfiguration extends Configuration {

	private ServletContext servletContext;

	public ServerConfiguration(String name, ServletContext servletContext) {
		super(name);
		this.servletContext = servletContext;
	}

	@Override
	protected StringBuilder getTemplate(String tagPath, String prefix) throws TemplateFileNotFoundException {
		if (Strings.isNotBlank(tagPath)) {
			// do not use File.separator for win
			String filePath = prefix + getDelimiter() + tagPath;

			String cacheKey = this.name + filePath;
			if (!ResourceCache.getInstance().containsTemplate(cacheKey)) {
				// not cached
				try {
					String tagContent = null;
					if (this.getSettings().getRealPath() != null) {
						// read from path (explicit path set in config)
						filePath = this.getSettings().getRealPath() + filePath;
						tagContent = Files.readString(filePath);
					} else {
						// read as resource
						tagContent = getServletResourceAsString(filePath);
					}
					if (tagContent == null) {
						throw new TemplateFileNotFoundException("File " + filePath + " not found.");
					}
					// add to cache
					StringBuilder tagContentStringBuilder = new StringBuilder(tagContent);

					ResourceCache.getInstance().putTemplate(cacheKey, tagContentStringBuilder);
					// keep the method here in case we disable cache
					return tagContentStringBuilder;
				} catch (TemplateFileAccessException e) {
					throw new TemplateFileNotFoundException("File " + filePath + " not found.", e);
				} catch (AFFileNotFoundException e) {
					throw new TemplateFileNotFoundException("File " + filePath + " not found.", e);
				}
			}
			return ResourceCache.getInstance().getTemplate(cacheKey);
		} else {
			return null;
		}
	}

	private String getServletResourceAsString(String path) throws AFFileNotFoundException {
		// not cached yet
		InputStream is = null;
		try {
			is = servletContext.getResourceAsStream(path);
			if (is != null) {
				return Files.readInputStream(is);
			} else {
				return null;
			}
		} catch (Exception e) {
			// null pointer
			throw new AFFileNotFoundException("Cannot get resource from " + path, e);
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				// ignore since commons io does the same
				// See org.apache.commons.io.IOUtils.java#IOUtils.closeQuietly
			}
		}
	}

	@Override
	public String getAbsolutePath() {
		if (this.getSettings().getRealPath() != null) {
			return this.getSettings().getRealPath();
		} else {
			return getServletPath(getDelimiter());
		}
	}

	@Override
	protected String getDelimiter() {
		return "/";
	}

	private String getServletPath(String path) {
		return servletContext.getRealPath(path);
	}
}
