/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.configuration.guards.ConditionGuard;
import com.codingcrayons.aspectfaces.configuration.guards.Guard;
import com.codingcrayons.aspectfaces.configuration.guards.VarGuard;
import com.codingcrayons.aspectfaces.el.AFContext;
import com.codingcrayons.aspectfaces.el.ELUtil;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.util.Collections;

@SuppressWarnings("unchecked")
public class Mapping {

	private final String name;
	private final String defaultTagPath;
	private final List<Guard> guards;
	private final Map<String, String> variables;

	public Mapping(String name, String defaultTagPath) {
		this.name = name;
		this.defaultTagPath = defaultTagPath;
		this.guards = new ArrayList<Guard>();
		this.variables = new HashMap<String, String>();
	}

	public void addVariable(String name, String value) {
		this.variables.put(name, value);
	}

	public void addVariables(Map<String, String> variables) {
		this.variables.putAll(variables);
	}

	public List<Variable> getVariables() {
		List<Variable> vars = new ArrayList<Variable>();
		for (Entry<String, String> entry : this.variables.entrySet()) {
			vars.add(new Variable(entry.getKey(), entry.getValue()));
		}
		return vars;
	}

	public void addGuards(List<Guard> guards) {
		this.guards.addAll(guards);
	}

	public void addGuard(Guard guard) {
		this.guards.add(guard);
	}

	public void addConditionGuard(String expression, String tagPath, String[] formRoles, String[] userRoles) {
		this.guards.add(new ConditionGuard(expression, tagPath, formRoles, userRoles));
	}

	public void addVarGuard(String var, String tagPath, String[] formRoles, String[] userRoles) {
		this.guards.add(new VarGuard(var, tagPath, formRoles, userRoles));
	}

	public String getName() {
		return name;
	}

	public String getTagPath(MetaProperty property, Context context) {
		List<Variable> localVars = Collections.concatLists(context.getVariableList(), property.getConfigVariables());
		AFContext variableContext = ELUtil.makeVariableContext(localVars);

		for (Guard guard : this.guards) {
			if (guard.evaluate(property, context, variableContext)) {
				return guard.getTagPath();
			}
		}
		return this.defaultTagPath;
	}
}
