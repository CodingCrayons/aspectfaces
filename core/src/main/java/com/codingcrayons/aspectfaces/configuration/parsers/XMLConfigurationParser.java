/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.Mapping;
import com.codingcrayons.aspectfaces.configuration.guards.ConditionGuard;
import com.codingcrayons.aspectfaces.configuration.guards.Guard;
import com.codingcrayons.aspectfaces.configuration.guards.VarGuard;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLConfigurationParser implements ConfigurationParser {

	private static final Logger LOGGER = LoggerFactory.getLogger(XMLConfigurationParser.class);

	@Override
	public void parseConfiguration(File file, Configuration configuration)
		throws ConfigurationParsingException, ConfigurationFileNotFoundException {

		try {
			this.parseConfiguration(new FileInputStream(file), configuration);
		} catch (FileNotFoundException e) {
			throw new ConfigurationFileNotFoundException(file.getAbsolutePath() + " file not found.", e);
		}
	}

	@Override
	public void parseConfiguration(InputStream in, Configuration configuration) throws ConfigurationParsingException {

		LOGGER.trace("Parsing configuration: {}", configuration.getName());

		SAXReader reader = new SAXReader();
		Document document;
		try {
			document = reader.read(in);
		} catch (DocumentException e) {
			throw new ConfigurationParsingException(e);
		}
		Element root = document.getRootElement();

		@SuppressWarnings("rawtypes")
		Iterator iterator = root.elementIterator();

		while (iterator.hasNext()) {
			Element element = (Element) iterator.next();
			this.processElement(element, configuration);
		}
	}

	protected void processElement(Element element, Configuration configuration) throws ConfigurationParsingException {

		if (element.getName().equals("mapping")) {
			for (Mapping mapping : this.parseMapping(element)) {
				configuration.addMapping(mapping);
			}
		} else if (element.getName().equals("ignore-fields")) {
			configuration.resetIgnoreFields();
			@SuppressWarnings("rawtypes")
			Iterator ifIterator = element.elementIterator();
			while (ifIterator.hasNext()) {
				Element subElement = (Element) ifIterator.next();
				if (subElement.getName().equals("name")) {
					configuration.addIgnoreField(subElement.getTextTrim());
				}
			}
		} else if (element.getName().equals("ignoring-annotations")) {
			configuration.resetIgnoringAnnotations();
			@SuppressWarnings("rawtypes")
			Iterator iaIterator = element.elementIterator();
			while (iaIterator.hasNext()) {
				Element subElement = (Element) iaIterator.next();
				if (subElement.getName().equals("name")) {
					configuration.addIgnoringAnnotations(subElement.getTextTrim());
				}
			}
		}
	}

	protected List<Mapping> parseMapping(Element root) throws ConfigurationParsingException {
		List<String> types = new ArrayList<String>(); // at least one required
		String tag = null; // required
		List<Guard> guards = new ArrayList<Guard>();
		Map<String, String> variables = new HashMap<String, String>();

		@SuppressWarnings("rawtypes")
		Iterator iterator = root.elementIterator();
		while (iterator.hasNext()) {
			Element element = (Element) iterator.next();
			if (element.getName().equals("type")) {
				types.add(element.getTextTrim());
			} else if (element.getName().equals("default")) {
				@SuppressWarnings("rawtypes")
				Iterator attributeIterator = element.attributeIterator();
				while (attributeIterator.hasNext()) {
					Attribute attribute = (Attribute) attributeIterator.next();
					if (attribute.getName().equals("tag")) {
						tag = attribute.getValue().trim();
					} else {
						variables.put(attribute.getName(), attribute.getValue());
					}
				}
			} else if (element.getName().equals("var")) {
				guards.add(this.parseVarGuards(element));
			} else if (element.getName().equals("condition")) {
				guards.add(this.parseConditionGuards(element));
			} else {
				throw new ConfigurationParsingException("Configuration error: " + element.getName() + " element is "
					+ "not approved in element mapping!");
			}
		}

		if (types.isEmpty()) {
			throw new ConfigurationParsingException(
				"Configuration error: element type must be defined in element mapping at least once!");
		}
		if (tag == null) {
			throw new ConfigurationParsingException(
				"Configuration error: element default with attribute tag must be defined in element mapping!");
		}

		List<Mapping> mappings = new ArrayList<Mapping>();

		for (String name : types) {
			Mapping mapping = new Mapping(name, tag);
			mapping.addVariables(variables);
			mapping.addGuards(guards);

			mappings.add(mapping);
		}

		return mappings;
	}

	protected Guard parseVarGuards(Element root) throws ConfigurationParsingException {
		String var = null;
		String tagPath = null;
		String[] profiles = null;
		String[] userRoles = null;

		@SuppressWarnings("rawtypes")
		Iterator attributeIterator = root.attributeIterator();
		while (attributeIterator.hasNext()) {
			Attribute attribute = (Attribute) attributeIterator.next();
			if (attribute.getName().equals("name")) {
				var = attribute.getValue();
			} else if (attribute.getName().equals("tag")) {
				tagPath = attribute.getValue().trim();
			} else if (attribute.getName().equals("user-roles")) {
				userRoles = attribute.getValue().split("\\s");
			} else if (attribute.getName().equals("profiles")) {
				profiles = attribute.getValue().split("\\s");
			} else {
				throw new ConfigurationParsingException("Configuration error: " + attribute.getName() + " attribute "
					+ "is not approved in var-guard element!");
			}
		}
		if (var == null) {
			throw new ConfigurationParsingException(
				"Configuration error: name attribute must be defined in element var!");
		}
		if (tagPath == null) {
			throw new ConfigurationParsingException(
				"Configuration error: tag attribute must be defined in element var!");
		}
		return new VarGuard(var, tagPath, profiles, userRoles);
	}

	protected Guard parseConditionGuards(Element root) throws ConfigurationParsingException {
		String expression = null;
		String tagPath = null;
		String[] profiles = null;
		String[] userRoles = null;

		@SuppressWarnings("rawtypes")
		Iterator attributeIterator = root.attributeIterator();
		while (attributeIterator.hasNext()) {
			Attribute attribute = (Attribute) attributeIterator.next();
			if (attribute.getName().equals("expression")) {
				expression = attribute.getValue();
			} else if (attribute.getName().equals("tag")) {
				tagPath = attribute.getValue().trim();
			} else if (attribute.getName().equals("user-roles")) {
				userRoles = attribute.getValue().split("\\s");
			} else if (attribute.getName().equals("profiles")) {
				profiles = attribute.getValue().split("\\s");
			} else {
				throw new ConfigurationParsingException("Configuration error: " + attribute.getName() + " attribute "
					+ "is not approved in condition element!");
			}
		}
		if (expression == null) {
			throw new ConfigurationParsingException(
				"Configuration error: expression attribute must be defined in element condition!");
		}
		if (tagPath == null) {
			throw new ConfigurationParsingException(
				"Configuration error: tag attribute must be defined in element condition!");
		}
		return new ConditionGuard(expression, tagPath, profiles, userRoles);
	}
}
