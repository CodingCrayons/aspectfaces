/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration.guards;

import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.el.AFContext;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.util.Arrays;

public abstract class AbstractGuard implements Guard {

	protected final String tagPath;

	protected final String[] profiles;
	protected final String[] userRoles;

	public AbstractGuard(String tagPath, String[] profiles, String[] userRoles) {
		this.tagPath = tagPath;
		this.profiles = profiles;
		this.userRoles = userRoles;
	}

	@Override
	public String getTagPath() {
		return this.tagPath;
	}

	protected boolean evaluateRoles(Context context) {
		boolean profilesOk = true;
		if (this.profiles != null) {
			if (context.getProfiles() == null) {
				return false;
			}
			profilesOk = Arrays.intersects(this.profiles, context.getProfiles());
		}

		boolean userRolesOk = true;
		if (this.userRoles != null) {
			if (context.getRoles() == null) {
				return false;
			}
			userRolesOk = Arrays.intersects(this.userRoles, context.getRoles());
		}

		return profilesOk && userRolesOk;
	}

	@Override
	public boolean evaluate(MetaProperty metaProperty, Context context, AFContext variableContext) {
		throw new UnsupportedOperationException("Called undefined method");
	}

	@Override
	public boolean evaluate(MetaProperty metaProperty, Context context) {
		throw new UnsupportedOperationException("Called undefined method");
	}
}
