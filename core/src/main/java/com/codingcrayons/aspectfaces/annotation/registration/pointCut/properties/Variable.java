/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Variable {
	private final String name;
	private final Object value;
	private final ExtendedVariableValue extendedVariableValue;
	private final boolean redefinable;
	private final Set<VarType> type;

	public enum VarType {
		TEMPLATE, CONFIG
	}

	;

	public Variable(String name, Object value) {
		this(name, value, true, new VarType[]{VarType.CONFIG, VarType.TEMPLATE});
	}

	public Variable(String name, Object value, VarType type) {
		this(name, value, true, new VarType[]{type});
	}

	public Variable(String name, Object value, boolean redefinable) {
		this(name, value, redefinable, new VarType[]{VarType.CONFIG, VarType.TEMPLATE});
	}

	public Variable(String name, Object value, boolean redefinable, VarType type) {
		this(name, value, redefinable, new VarType[]{type});
	}

	public Variable(String name, Object value, boolean redefinable, VarType[] type) {
		this.name = name;
		this.value = value;
		this.redefinable = redefinable;
		this.type = new HashSet<VarType>(Arrays.asList(type));
		this.extendedVariableValue = new ExtendedVariableValue(this);
	}

	public String getName() {
		return name;
	}

	public Object getValue() {
		return value;
	}

	public ExtendedVariableValue getExtendedValue() {
		return extendedVariableValue;
	}

	public boolean isRedefinable() {
		return redefinable;
	}

	public boolean isTemplate() {
		return type.contains(VarType.TEMPLATE);
	}

	public boolean isConfig() {
		return type.contains(VarType.CONFIG);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Variable other = (Variable) obj;
		if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
			return false;
		}
		if ((this.value == null) ? (other.value != null) : !this.value.equals(other.value)) {
			return false;
		}
		if ((!this.type.equals(other.type))) {
			return false;
		}
		if (this.redefinable != other.redefinable) {
			return false;
		}
		return true;
	}
}
