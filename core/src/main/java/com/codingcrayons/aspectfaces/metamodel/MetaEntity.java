/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.metamodel;

import java.util.HashSet;
import java.util.Set;

public class MetaEntity {

	private String name;
	private String simpleName;
	private long order = 0;
	private Set<MetaProperty> propertySet = new HashSet<MetaProperty>();

	public MetaEntity(String name, String simpleName) {
		this.name = name;
		this.simpleName = simpleName;
	}

	public MetaEntity(String name, String simpleName, long order) {
		this.name = name;
		this.simpleName = simpleName;
		this.order = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public void setSimpleName(String simpleName) {
		this.simpleName = simpleName;
	}

	public Long getOrder() {
		return order;
	}

	public void setOrder(Long order) {
		this.order = order;
	}

	public Set<MetaProperty> getPropertySet() {
		return propertySet;
	}

	public void setPropertySet(Set<MetaProperty> propertySet) {
		this.propertySet = propertySet;
	}
}
