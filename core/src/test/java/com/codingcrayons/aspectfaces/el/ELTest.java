/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.el;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ELTest {

	public static class TestObject {
		private String value = "Hello";

		public String getValue() {
			return value;
		}

		public String print(String end) {
			return value + " " + end;
		}
	}

	@DataProvider
	public Object[][] elProvider() {
		String person = "personValue";
		String test = "TestValue";
		String instance = "instance.value";
		int six = 6;
		double sevenPointTwo = 7.2;
		TestObject testObject = new TestObject();
		boolean bool = true;

		List<Variable> vars = new ArrayList<Variable>(7);
		vars.add(new Variable("person", person));
		vars.add(new Variable("test", test));
		vars.add(new Variable("instance", instance));
		vars.add(new Variable("six", six));
		vars.add(new Variable("sevenPointTwo", sevenPointTwo));
		vars.add(new Variable("testObject", testObject));
		vars.add(new Variable("bool", bool));

		AFContext variableContext = ELUtil.makeVariableContext(vars);
		AFContext extendedVariableContext = ELUtil.makeExtendedVariableContext(vars);

		return new Object[][]{
			new Object[]{variableContext, "#{person}", String.class, person},
			new Object[]{variableContext, "#{test}", String.class, test},
			new Object[]{variableContext, "#{instance}", String.class, instance},
			new Object[]{variableContext, "#{six}", Integer.class, six},
			new Object[]{variableContext, "#{sevenPointTwo}", Double.class, sevenPointTwo},
			new Object[]{variableContext, "#{testObject.value}", String.class, testObject.getValue()},
			new Object[]{variableContext, "#{testObject.print('John')}", String.class, testObject.print("John")},
			new Object[]{variableContext, "#{bool}", Boolean.class, bool},
			new Object[]{variableContext, "#{six * 2}", Integer.class, six * 2},
			new Object[]{variableContext, "#{sevenPointTwo - six}", Double.class, sevenPointTwo - six},
			new Object[]{extendedVariableContext, "#{person.firstToUpper()}", String.class, "PersonValue"},
			new Object[]{extendedVariableContext, "#{test.firstToLower()}", String.class, "testValue"}
		};
	}

	@Test(dataProvider = "elProvider")
	public void testAFEL(AFContext variableContext, String elExpression, Class<?> resultClass, Object expected) {
		assertEL(variableContext, elExpression, resultClass, expected);
	}

	@DataProvider
	public Object[][] unresolvedVariableProvider() {
		List<Variable> vars = new ArrayList<Variable>(2);
		vars.add(new Variable("t", true));
		vars.add(new Variable("f", false));

		AFContext variableContext = ELUtil.makeVariableContext(vars);

		return new Object[][]{
			new Object[]{variableContext, "#{person}", String.class, ""},
			new Object[]{variableContext, "#{person}", Boolean.class, false},
			new Object[]{variableContext, "#{t ? 'x' : person}", String.class, "x"},
			new Object[]{variableContext, "#{f ? 'x' : person}", String.class, ""},
			new Object[]{variableContext, "#{t ? person : 'x'}", Boolean.class, false},
			new Object[]{variableContext, "#{person ? 't' : 'f' }", String.class, "f"}
		};
	}

	@Test(dataProvider = "unresolvedVariableProvider")
	public void testUnresolvedVariable(AFContext variableContext, String elExpression, Class<?> resultClass, Object expected) {
		assertEL(variableContext, elExpression, resultClass, expected);
	}

	@Test
	public void testIndependentContexts() {
		AFContext variableContext = ELUtil.makeVariableContext(new ArrayList<Variable>(1) {
			{
				add(new Variable("person", "test"));
			}
		});
		assertEL(variableContext, "#{person}", String.class, "test");

		variableContext = ELUtil.makeVariableContext(Collections.<Variable>emptyList());
		assertEL(variableContext, "#{person}", String.class, "");
	}

	private void assertEL(AFContext variableContext, String elExpression, Class<?> resultClass, Object expected) {
		assertEquals(ELUtil.createValueExpression(variableContext, elExpression, resultClass).getValue(variableContext), expected);
	}
}
