/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.support.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.codingcrayons.aspectfaces.annotations.UiFormOrder;
import com.codingcrayons.aspectfaces.annotations.UiFormProfiles;
import com.codingcrayons.aspectfaces.annotations.UiTableOrder;
import com.codingcrayons.aspectfaces.annotations.UiText;

public class Owner implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String firstname;
	private String lastname;
	private String notice;
	private Date birthdate;
	private String phone;
	private String email;
	private Set<Car> cars = new HashSet<Car>(0);
	private Set<Address> addresses = new HashSet<Address>(0);
	private Short height;
	private Short weight;
	private Long version;

	public Owner() {
	}

	public Owner(Long id, String firstname, String lastname, Date birthdate, String email) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate = birthdate;
		this.email = email;
	}

	public Owner(Long id, String firstname, String lastname, String notice, Date birthdate, String phone, String email,
				 Set<Car> cars, Set<Address> addresses, Short height, Short weigth) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.notice = notice;
		this.birthdate = birthdate;
		this.phone = phone;
		this.email = email;
		this.cars = cars;
		this.addresses = addresses;
		this.height = height;
		this.weight = weigth;
	}

	@UiTableOrder(1)
	@UiFormProfiles({"edit"})
	@UiFormOrder(1)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@UiTableOrder(2)
	@UiFormOrder(2)
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@UiTableOrder(3)
	@UiFormOrder(3)
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@UiText(cols = 40, rows = 3)
	@UiFormOrder(5.5)
	public String getNotice() {
		return this.notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	@UiTableOrder(4)
	@UiFormOrder(4)
	@UiFormProfiles({"edit", "new"})
	public Date getBirthdate() {
		return this.birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	@UiTableOrder(6)
	@UiFormOrder(6)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@UiTableOrder(5)
	@UiFormOrder(5)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Car> getCars() {
		return this.cars;
	}

	public void setCars(Set<Car> cars) {
		this.cars = cars;
	}

	public Set<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	@UiTableOrder(7)
	@UiFormOrder(7)
	public Short getHeight() {
		return this.height;
	}

	public void setHeight(Short height) {
		this.height = height;
	}

	@UiTableOrder(8)
	@UiFormOrder(8)
	public Short getWeight() {
		return this.weight;
	}

	public void setWeight(Short weight) {
		this.weight = weight;
	}

	public Long getVersion() {
		return this.version != null ? this.version : 0;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
