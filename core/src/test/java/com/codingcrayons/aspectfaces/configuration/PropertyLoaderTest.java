/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.configuration;

import java.io.IOException;
import java.io.StringReader;

import com.codingcrayons.aspectfaces.exceptions.ConfigurationFileNotFoundException;
import com.codingcrayons.aspectfaces.exceptions.ConfigurationParsingException;
import com.codingcrayons.aspectfaces.properties.PropertyLoader;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class PropertyLoaderTest {

	@Test(expectedExceptions = ConfigurationFileNotFoundException.class)
	public void testNullValueOnNonExFile() throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		// non-existing property file
		PropertyLoader loader = new PropertyLoader("wheee");
		// throws here
		String test = loader.getProperty("test");
		assertNull(test);
	}

	@Test(expectedExceptions = ConfigurationFileNotFoundException.class)
	public void testDefaultValueOnNonExFile() throws ConfigurationFileNotFoundException, ConfigurationParsingException {
		// non-existing property file
		PropertyLoader loader = new PropertyLoader("wheee");
		// throws here
		String test = loader.getProperty("test", "test");
		assertEquals(test, "test");
	}

	@Test
	public void testNullValue() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test2=true"));
		String test = loader.getProperty("test");
		assertNull(test);
	}

	@Test
	public void testDefaultValue() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test2=true"));
		String test = loader.getProperty("test", "test");
		assertEquals(test, "test");
	}

	@Test
	public void testGettingProperty() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test=true"));
		String test = loader.getProperty("test");
		assertEquals(test, "true");
	}

	@Test
	public void testGettingPropertyNotNull() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test=true"));
		String test = loader.getProperty("test");
		assertNotNull(test);
	}

	@Test
	public void testGettingPropertyNull() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test=true"));
		String test = loader.getProperty("true");
		assertNull(test);
	}

	@Test
	public void testNotUsingDefaultValue() throws IOException {
		PropertyLoader loader = new PropertyLoader(new StringReader("test=true"));
		String test = loader.getProperty("test", "false");
		assertEquals(test, "true");
	}
}
