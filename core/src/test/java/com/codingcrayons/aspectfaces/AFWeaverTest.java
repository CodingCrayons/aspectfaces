/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces;

import java.io.File;

import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.ConfigurationStorage;
import com.codingcrayons.aspectfaces.configuration.StaticConfiguration;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

public class AFWeaverTest extends TestCase {

	private static final String CONFIG = "detail.config.xml";

	@Test
	public void testAddConfigurationByPath() {
		try {
			AFWeaver.addStaticConfiguration(getConfig(CONFIG));
			assertNotNull(ConfigurationStorage.getInstance().getConfiguration(CONFIG));
		} catch (Exception e) {
			fail("An unexpected exception: " + e.getMessage());
		}
	}

	@Test
	public void testAddConfigurationByFile() {
		try {
			File file = new File(getConfig(CONFIG));
			Configuration configuration = new StaticConfiguration(file.getName());
			AFWeaver.addConfiguration(configuration, file, true, true);
			assertNotNull(ConfigurationStorage.getInstance().getConfiguration(file.getName()));
		} catch (Exception e) {
			fail("An unexpected exception: " + e.getMessage());
		}
	}

	@AfterMethod
	public void tearDown() {
		// A little brain fuck, AFWeaver sets static properties
		AFWeaver.reset();
	}
}
