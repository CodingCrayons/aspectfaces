/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.variableResolver;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.annotation.registration.pointCut.properties.Variable;
import com.codingcrayons.aspectfaces.annotations.UiPattern;
import com.codingcrayons.aspectfaces.annotations.UiRequired;
import com.codingcrayons.aspectfaces.composition.UIFragmentComposer;
import com.codingcrayons.aspectfaces.configuration.Configuration;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.Mapping;
import com.codingcrayons.aspectfaces.configuration.StaticConfiguration;
import com.codingcrayons.aspectfaces.exceptions.EvaluatorException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;
import com.codingcrayons.aspectfaces.metamodel.MetaProperty;
import com.codingcrayons.aspectfaces.util.Collections;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

public class TagVariableResolverTest extends TestCase {

	@Test
	public void testResolve() {
		try {
			String template = "<util:inputText label=\"#{text['$entityBeanLower$.$field$']}\"\n"
				+ "edit=\"#{edit}\"\n"
				+ "value=\"#{$value$}\"\n"
				+ "required=\"$notNull$\"\n"
				+ "size=\"$size$\"\n"
				+ "title=\"#{text['tooltip.$entityBeanLower$.$field$']}\"\n"
				+ "pattern=\"'$javaPattern$'\"\n"
				+ "minlength=\"$empty minLength ? 'fck' : 'ee' $\"\n"
				+ "maxlength=\"$maxLength$\"\n"
				+ "rendered=\"#{empty render$field.value.toUpperCase()$ ? 'true' : render$field.firstToUpper()$}\"\n"
				+ "id=\"#{prefix}$field$\" />";

			String expected = "<util:inputText label=\"#{text['person.name']}\"\n" + "edit=\"#{edit}\"\n"
				+ "value=\"#{instance.name}\"\n" + "required=\"false\"\n" + "size=\"100\"\n"
				+ "title=\"#{text['tooltip.person.name']}\"\n" + "pattern=\"''\"\n" + "minlength=\"ee\"\n"
				+ "maxlength=\"\"\n" + "rendered=\"#{empty renderNAME ? 'true' : renderName}\"\n"
				+ "id=\"#{prefix}name\" />";

			List<Variable> vars = new ArrayList<Variable>();
			vars.add(new Variable("entityBeanLower", "person"));
			vars.add(new Variable("field", "name"));
			vars.add(new Variable("value", "instance.name"));
			vars.add(new Variable("notNull", "false"));
			vars.add(new Variable("size", "100"));
			vars.add(new Variable("minLength", null));

			TagVariableResolver resolver = new TagVariableResolver();
			StringBuilder output = resolver.resolve("Test.xhtml", template, "$", "$", vars);
			assertEquals(output.toString(), expected);
		} catch (TagParserException ex) {
			fail("An unexpected exception: " + ex.getMessage());
		}
	}

	@Test
	public void testResolveLong() {
		try {
			String template = "<util:inputText label=\"#{text['@{entityBeanLower}.@{field}']}\"\n"
				+ "edit=\"#{edit}\"\n"
				+ "value=\"#{@{value}}\"\n"
				+ "required=\"@{notNull}\"\n"
				+ "size=\"@{size}\"\n"
				+ "title=\"#{text['tooltip.@{entityBeanLower}.@{field}']}\"\n"
				+ "pattern=\"'@{javaPattern}'\"\n"
				+ "minlength=\"@{empty minLength ? 'fck' : 'ee' }\"\n"
				+ "maxlength=\"@{maxLength}\"\n"
				+ "rendered=\"#{empty render@{field.value.toUpperCase()} ? 'true' : render@{field.firstToUpper()}}\"\n"
				+ "id=\"#{prefix}@{field}\" />";

			String expected = "<util:inputText label=\"#{text['person.name']}\"\n" + "edit=\"#{edit}\"\n"
				+ "value=\"#{instance.name}\"\n" + "required=\"false\"\n" + "size=\"100\"\n"
				+ "title=\"#{text['tooltip.person.name']}\"\n" + "pattern=\"''\"\n" + "minlength=\"ee\"\n"
				+ "maxlength=\"\"\n" + "rendered=\"#{empty renderNAME ? 'true' : renderName}\"\n"
				+ "id=\"#{prefix}name\" />";

			List<Variable> vars = new ArrayList<Variable>();
			vars.add(new Variable("entityBeanLower", "person"));
			vars.add(new Variable("field", "name"));
			vars.add(new Variable("value", "instance.name"));
			vars.add(new Variable("notNull", "false"));
			vars.add(new Variable("size", "100"));
			vars.add(new Variable("minLength", null));

			TagVariableResolver resolver = new TagVariableResolver();
			StringBuilder output = resolver.resolve("Test.xhtml", template, "@{", "}", vars);
			assertEquals(output.toString(), expected);
		} catch (TagParserException ex) {
			fail("An unexpected exception: " + ex.getMessage());
		}
	}

	static class Person {
		String name;

		@UiRequired
		@UiPattern("\\s")
		public String getName() {
			return name;
		}
	}

	@Test
	public void testResolveSugar() throws Exception {

		String template = "<util:inputText label=\"#{text['$entityBean.shortClassName().firstToLower()$.$field$']}\"\n"
			+ "edit=\"#{edit}\"\n" + "value=\"#{$value$}\"\n" + "required=\"$required$\"\n" + "size=\"$size$\"\n"
			+ "title=\"#{text['tooltip.$entityBean$.$field$']}\"\n" + "pattern=\"'$javaPattern$'\"\n"
			+ "uipattern=\"'$pattern$'\"\n" + "minlength=\"$empty minLength ? 'fck' : 'ee' $\"\n"
			+ "maxlength=\"$maxLength$\"\n"
			+ "rendered=\"#{empty render$field.value.toUpperCase()$ ? 'true' : render$field.firstToUpper()$}\"\n"
			+ "id=\"#{prefix}$field$\" />\n\n" + "$myVar$ \n" + "$dataType$ \n" + "$DataType$ \n"
			+ "$fieldName$ \n" + "$FieldName$ \n" + "$className$ \n" + "$ClassName$ \n" + "$fullClassName$ \n"
			+ "$FullClassName$ \n" + "$value$ ";

		String expected = "<util:inputText label=\"#{text['tagVariableResolverTest$Person.name']}\"\n"
			+ "edit=\"#{edit}\"\n"
			+ "value=\"#{iPerson.name}\"\n"
			+ "required=\"true\"\n"
			+ "size=\"\"\n"
			+ "title=\"#{text['tooltip.com.codingcrayons.aspectfaces.variableResolver.TagVariableResolverTest$Person.name']}\"\n"
			+ "pattern=\"''\"\n" + "uipattern=\"'\\\\s'\"\n" + "minlength=\"fck\"\n" + "maxlength=\"\"\n"
			+ "rendered=\"#{empty renderNAME ? 'true' : renderName}\"\n" + "id=\"#{prefix}name\" />\n\n"
			+ "YES! \n" + "string \n" + "String \n" + "name \n" + "Name \n" + "person \n" + "Person \n"
			+ "com.codingcrayons.aspectfaces.variableresolver.tagvariableresolvertest$person \n"
			+ "com.codingcrayons.aspectfaces.variableResolver.TagVariableResolverTest$Person \n" + "iPerson.name ";

		JavaInspector inspector = new JavaInspector(Person.class);
		// context
		Context context = new Context();
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("myVar", "YES!");
		context.setVariables(variables);

		AFWeaver.registerAllAnnotations();
		AFWeaver.setOpenVariableBoundaryIdentifier("$");
		AFWeaver.setCloseVariableBoundaryIdentifier("$");

		// set config (ignores fields)
		String CONFIG = "detail.config.xml";
		File file = new File(getResource("configuration/" + CONFIG));
		Configuration configuration = new StaticConfiguration(file.getName());
		configuration.addMapping(new Mapping("String", "path"));
		context.setConfiguration(configuration);

		List<MetaProperty> metaProperties = inspector.inspect(context);
		UIFragmentComposer composer = new UIFragmentComposer();
		composer.addAllFields(metaProperties);
		for (MetaProperty property : metaProperties) {
			@SuppressWarnings("unchecked")
			List<Variable> localVars = Collections.concatLists(context.getVariableList(),
				property.getTemplateVariables());
			TagVariableResolver vr = new TagVariableResolver();
			try {
				StringBuilder result = vr.resolve("FRAGMENT_NAME", template,
					AFWeaver.getOpenVariableBoundaryIdentifier(), AFWeaver.getCloseVariableBoundaryIdentifier(),
					localVars);
				assertEquals(result.toString(), expected);
			} catch (TagParserException e) {
				throw new EvaluatorException(e.getMessage(), e);
			}
		}
	}
}
