/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.codingcrayons.aspectfaces.cache;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import com.codingcrayons.aspectfaces.AFWeaver;
import com.codingcrayons.aspectfaces.TestCase;
import com.codingcrayons.aspectfaces.configuration.Context;
import com.codingcrayons.aspectfaces.configuration.Settings;
import com.codingcrayons.aspectfaces.exceptions.AFException;
import com.codingcrayons.aspectfaces.exceptions.CacheProviderNotSetException;
import com.codingcrayons.aspectfaces.metamodel.JavaInspector;
import com.codingcrayons.aspectfaces.support.model.Administrator;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class SimpleCacheTest extends TestCase {

	private static final String regionA = "region-a";
	private static final String regionB = "region-b";
	private static final String keyA = "key-a";
	private static final String keyB = "key-b";
	private static final String keyAregA = "key-a-reg-a";
	private static final String keyBregA = "key-b-reg-a";
	private static final String keyAregB = "key-a-reg-b";
	private static final String keyBregB = "key-b-reg-b";
	private static final String valueA = "value-a";
	private static final String valueB = "value-b";
	private static final String valueAkeyAregA = "value-a-key-a-reg-a";
	private static final String valueBkeyBregA = "value-b-key-b-reg-a";
	private static final String valueAkeyAregB = "value-a-key-a-reg-b";
	private static final String valueBkeyBregB = "value-b-key-b-reg-b";

	private static final String CONFIG = "detail.config.xml";

	private SimpleCache cache;

	@BeforeMethod
	public void setUp() {
		cache = new SimpleCache();

		cache.put(keyA, valueA);
		cache.put(keyB, valueB);

		cache.put(regionA, keyAregA, valueAkeyAregA);
		cache.put(regionA, keyBregA, valueBkeyBregA);

		cache.put(regionB, keyAregB, valueAkeyAregB);
		cache.put(regionB, keyBregB, valueBkeyBregB);
	}

	@Test
	public void testPut() {
		assertEquals(cache.get(keyA), valueA);
		assertEquals(cache.get(keyB), valueB);

		assertEquals(cache.get(regionA, keyAregA), valueAkeyAregA);
		assertEquals(cache.get(regionA, keyBregA), valueBkeyBregA);

		assertEquals(cache.get(regionB, keyAregB), valueAkeyAregB);
		assertEquals(cache.get(regionB, keyBregB), valueBkeyBregB);

		assertNull(cache.get(keyAregA));
		assertNull(cache.get(keyBregA));
		assertNull(cache.get(keyAregB));
		assertNull(cache.get(keyBregB));

		assertNull(cache.get(regionA, keyA));
		assertNull(cache.get(regionA, keyB));
		assertNull(cache.get(regionA, keyAregB));
		assertNull(cache.get(regionA, keyBregB));

		assertNull(cache.get(regionB, keyA));
		assertNull(cache.get(regionB, keyB));
		assertNull(cache.get(regionB, keyAregA));
		assertNull(cache.get(regionB, keyBregA));
	}

	@Test
	public void testChangeDefaultRegion() {
		cache.setDefaultRegion(regionA);

		assertEquals(cache.get(keyAregA), valueAkeyAregA);
		assertEquals(cache.get(keyBregA), valueBkeyBregA);

		assertNull(cache.get(keyA));
		assertNull(cache.get(keyB));

		cache.setDefaultRegion(null);

		assertEquals(cache.get(keyA), valueA);
		assertEquals(cache.get(keyB), valueB);

		assertNull(cache.get(keyAregA));
		assertNull(cache.get(keyBregA));
	}

	@Test
	public void testRemove() {
		assertEquals(cache.get(keyA), valueA);
		cache.remove(keyA);
		assertNull(cache.get(keyA));
		assertEquals(cache.get(keyB), valueB);

		assertEquals(cache.get(regionA, keyAregA), valueAkeyAregA);
		cache.remove(regionA, keyAregA);
		assertNull(cache.get(regionA, keyAregA));
		assertEquals(cache.get(regionA, keyBregA), valueBkeyBregA);

		assertEquals(cache.get(regionB, keyAregB), valueAkeyAregB);
		cache.remove(regionB, keyAregB);
		assertNull(cache.get(regionB, keyAregB));
		assertEquals(cache.get(regionB, keyBregB), valueBkeyBregB);

		cache.remove("non-region", "key");
	}

	@Test
	public void testClearRegion() {
		assertEquals(cache.get(keyA), valueA);
		assertEquals(cache.get(keyB), valueB);
		cache.clear(null);
		assertNull(cache.get(keyA));
		assertNull(cache.get(keyB));

		assertEquals(cache.get(regionA, keyAregA), valueAkeyAregA);
		assertEquals(cache.get(regionA, keyBregA), valueBkeyBregA);
		cache.clear(regionA);
		assertNull(cache.get(regionA, keyAregA));
		assertNull(cache.get(regionA, keyBregA));

		assertEquals(cache.get(regionB, keyAregB), valueAkeyAregB);
		assertEquals(cache.get(regionB, keyBregB), valueBkeyBregB);
		cache.clear(regionB);
		assertNull(cache.get(regionB, keyAregB));
		assertNull(cache.get(regionB, keyBregB));
	}

	@Test
	public void testClear() {
		assertEquals(cache.get(keyA), valueA);
		assertEquals(cache.get(keyB), valueB);

		assertEquals(cache.get(regionA, keyAregA), valueAkeyAregA);
		assertEquals(cache.get(regionA, keyBregA), valueBkeyBregA);

		assertEquals(cache.get(regionB, keyAregB), valueAkeyAregB);
		assertEquals(cache.get(regionB, keyBregB), valueBkeyBregB);

		final String newRegion = "newRegion";
		cache.setDefaultRegion(newRegion);
		cache.clear();

		assertNull(cache.get(keyA));
		assertNull(cache.get(keyB));

		assertNull(cache.get(regionA, keyAregA));
		assertNull(cache.get(regionA, keyBregA));

		assertNull(cache.get(regionB, keyAregB));
		assertNull(cache.get(regionB, keyBregB));

		assertEquals(cache.getDefaultRegion(), newRegion);
	}

	@Test
	public void testReset() {
		assertEquals(cache.get(keyA), valueA);
		assertEquals(cache.get(keyB), valueB);

		assertEquals(cache.get(regionA, keyAregA), valueAkeyAregA);
		assertEquals(cache.get(regionA, keyBregA), valueBkeyBregA);

		assertEquals(cache.get(regionB, keyAregB), valueAkeyAregB);
		assertEquals(cache.get(regionB, keyBregB), valueBkeyBregB);

		final String defaultRegion = cache.getDefaultRegion();
		cache.setDefaultRegion("differentRegion");
		cache.reset();

		assertNull(cache.get(keyA));
		assertNull(cache.get(keyB));

		assertNull(cache.get(regionA, keyAregA));
		assertNull(cache.get(regionA, keyBregA));

		assertNull(cache.get(regionB, keyAregB));
		assertNull(cache.get(regionB, keyBregB));

		assertEquals(cache.getDefaultRegion(), defaultRegion);
	}

	@Test
	public void testCacheIntegration() throws AFException, FileNotFoundException {
		AFWeaver.registerAllAnnotations();

		AFWeaver.init(new FileInputStream(getResource("aspectfaces.properties")));

		SimpleCache afCache = new SimpleCache();
		AFWeaver.setDefaultCacheProvider(afCache);

		AFWeaver.addStaticConfiguration(getConfig(CONFIG));
		AFWeaver afWeaver = new AFWeaver(CONFIG, new JavaInspector(Administrator.class));

		Context context = new Context(Administrator.class.getName(), null, null, null, false);

		String fragment = afWeaver.generate(context, keyA);
		assertEquals(afCache.get(keyA), fragment);
		fragment = afWeaver.generate(context, keyA);
		assertEquals(afCache.get(keyA), fragment);

		fragment = afWeaver.generate(context, new Settings(), keyB);
		assertEquals(afCache.get(keyB), fragment);
		fragment = afWeaver.generate(context, new Settings(), keyB);
		assertEquals(afCache.get(keyB), fragment);

		fragment = afWeaver.generate(context, regionA, keyAregA);
		assertEquals(afCache.get(regionA, keyAregA), fragment);
		fragment = afWeaver.generate(context, regionA, keyAregA);
		assertEquals(afCache.get(regionA, keyAregA), fragment);

		fragment = afWeaver.generate(context, new Settings(), regionB, keyAregB);
		assertEquals(afCache.get(regionB, keyAregB), fragment);
		fragment = afWeaver.generate(context, new Settings(), regionB, keyAregB);
		assertEquals(afCache.get(regionB, keyAregB), fragment);
	}

	@Test(expectedExceptions = CacheProviderNotSetException.class)
	public void testCacheIntegrationFail() throws AFException {
		Context context = new Context(Administrator.class.getName(), null, null, null, true);
		AFWeaver.addStaticConfiguration(getConfig(CONFIG));
		AFWeaver.setDefaultCacheProvider(null);
		AFWeaver afWeaver = new AFWeaver(CONFIG);
		afWeaver.generate(context, keyA);
	}

	@AfterMethod
	public void tearDown() {
		AFWeaver.reset();
	}
}
