/*
 * Copyright (C) 2011-2014 CodingCrayons s.r.o.
 * All rights reserved.
 * Contact: CodingCrayons s.r.o. (info@codingcrayons.com)
 *
 * This file is part of AspectFaces.
 *
 * AspectFaces is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

function colorPreview(value, invoker, objectID) {

    var invokerName;

    if (invoker.nodeName != null) { /* simple */
        invokerName = (invoker.nodeName.toLowerCase() == "input") ? invoker.id : "undefined";
    } else { /* seam rich */
        invokerName = invoker.input.name;

    }


    var prefix = parseParentTree(invokerName);
    var object = document.getElementById(prefix + ':' + objectID);
    if (object != null) {
        /* get existing color and add new */
        var color = object.style.backgroundColor;

        rgb = rgbConvert(color);
        switch (parseColor(invokerName)) {
            case 'r':
                rgb[0] = value;
                break;
            case 'g':
                rgb[1] = value;
                break;
            case 'b':
                rgb[2] = value;
                break;
        }
        ;

        var colorText = "rgb(" + rgb[0] + ',' + rgb[1] + ',' + rgb[2] + ")";
        object.style.backgroundColor = '#' + dec2Hex(rgb[0]) + dec2Hex(rgb[1]) + dec2Hex(rgb[2]);
    }
}

function parseParentTree(text) {
    var indexToBreak = text.lastIndexOf(':');
    return text.substring(0, indexToBreak);
}

function parseColor(text) {
    var indexToBreak = text.lastIndexOf(':');
    var id = text.substring(indexToBreak, text.length);
    return id.charAt(1);
}

function rgbConvert(str) {
    var arrayInt = new Array();
    if (str.substring(0, 1) == '#') { /* #DDEEBB */
        arrayInt[0] = parseInt(hex2Dec(str.substring(1, 3)));
        arrayInt[1] = parseInt(hex2Dec(str.substring(3, 5)));
        arrayInt[2] = parseInt(hex2Dec(str.substring(5, 7)));
    } else {
        str = str.replace(/rgb\(|\)/g, "").split(",");
        arrayInt[0] = parseInt(str[0], 10);
        arrayInt[1] = parseInt(str[1], 10);
        arrayInt[2] = parseInt(str[2], 10);
    }
    return arrayInt;


}

function dec2Hex(dec) {
    var hexChars = "0123456789ABCDEF";
    var a = dec % 16;
    var b = (dec - a) / 16;
    hex = "" + hexChars.charAt(b) + hexChars.charAt(a);
    return hex;
}
function hex2Dec(hexVal) {
    var hexChars = "0123456789ABCDEF";
    hexVal = hexVal.toUpperCase();
    var DecVal = 0;
    var temp = hexVal.substring(0, 1);
    DecVal = (hexChars.indexOf(temp) * 16);
    temp = hexVal.substring(1);
    DecVal += hexChars.indexOf(temp);
    return DecVal;
}
