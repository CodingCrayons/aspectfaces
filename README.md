# AspectFaces

You may have heard that approximately 50% of the time devoted to application development is consumed by user interface (UI). In addition, there exist significant dependencies to the backend part of the application and many cross-cutting concerns. It is a common practice to restate and extend backend information in presentation and mix concerns together. This results with strong coupling and hard maintenance. This all ends up with error-prone and tedious work related to UI development and maintenance.

Can reduce these efforts devoted to UI development? Can we reduce the overall time and budget. Well, if we can reduce the maintenance efforts we can get even more!

We present a new approach for UI development that reduces development efforts to minimum. Each concern is an individual (layout, security, validation, widget, etc.), and backend information are machine inspected. This way you only define your presentation once and it is distributed to UI as you aimed, furthermore it always adapt to your latest backend without any efforts!

## Usage
To generate editable or read-only form for given class just write following line:

    <af:ui instance="#{bean.entity}" edit="true" />

Besides that you need to define standard configuration as is described in documentation. For that and further information look at [wiki](http://wiki.aspectfaces.com/display/af/AspectFaces).

## Modules
 * **core**: Core of AspectFaces framework, implements inspection and aspect weaving
 * **annotation-descriptors**: JPA and Hibernate annotations inspectors
 * **javaee-connector**: Injection into J2EE process, defines `AspectFacesListener`
 
## External Resources
 * [Homepage](http://aspectfaces.com/)
 * [Demo](http://demo.aspectfaces.com/)
 * [Showcase](http://showcase.aspectfaces.com/)
 * [Wiki](http://wiki.aspectfaces.com/display/af/AspectFaces)
 * [Repository](https://bitbucket.org/CodingCrayons/aspectfaces/)
 * [Continuous Integration](http://jenkins.codingcrayons.com/job/AspectFaces/)

## Maven
AspectFaces Maven repository is located on [http://maven.codingcrayons.com/](http://maven.codingcrayons.com/) and can be included
into your `pom.xml` files.

    <repository>
	    <id>codingcrayons-repository</id>
	    <name>CodingCrayons Maven Repository</name>
	    <url>http://maven.codingcrayons.com/content/groups/public/</url>
    </repository>

To include AspectFaces into your JSF project you have to include the following dependency. If you do not need JSF integration, you can include just subset of this 
meta-dependency.

    <dependency>
        <groupId>com.codingcrayons.aspectfaces</groupId>
        <artifactId>javaee-jsf2</artifactId>
        <version>${aspectfaces.version}</version>
        <type>pom</type>
    </dependency>

## Licence
Framework is published under LGPL v3.
